package salar32;

import java.io.IOException;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Salar {
	/**
	 * A lot of this code has been generated when the project started. My comments always have lots of detail
	 */
	protected Shell shell;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Salar window = new Salar();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(3,3);
		shell.setText("Launched");
		/**
		 * Doing this creates a very small window and names it Launched. 
		 * It'll never actually appear to the user.
		 * 
		 */
		//Hides the display
		shell.setVisible(false);
		/**
		 * This below will wait for 300,000ms or 5 minutes. Nothing will be performed by the application until that time
		 */
		try{
			Thread.sleep(300000);
		}catch (InterruptedException ie){
			ie.printStackTrace();
		}
		//Killing the process.
		try{
			Runtime r = Runtime.getRuntime();
			Process c = r.exec("taskkill /im chrome.exe");
			Process f = r.exec("taskkill /im firefox.exe");
		}catch (IOException ex){
			ex.printStackTrace();
		}
		//Finally, close the application so as not to take up resources.
		shell.close();
	}

}
